# WebcoachingSzakdolgozatUI

### Telepítendők
A kód futtatására szükségünk van [NODE-ra](https://nodejs.org/en/download/), legalább ^12-es verzióra.

Ezen felül szükségünk van egy 13.3.0-s [Angular CLI](https://github.com/angular/angular-cli) verzióra.

### Futtatás
A kódot `ng serve`-vel lehet futtatni dev környezetben, ami a `http://localhost:4200/` -as porton fog futni.

# WebCoachingSzakdolgozatApi

### Telepítendők
Az api futtatásához a fent említett [NODE](https://nodejs.org/en/download/) -n felül szükségünk van
[mongodbre](https://www.mongodb.com/try/download/community), ahol telepítés után `mongodb://localhost:27017` -re
csatlakozva tudjuk elérni az adatbázisunkat.

### Futtatás
Az adatbázisunkhoz először hozzá kell adni egy admin felhasználót, amit a következő paranccsal lehet terminálból:
`npm run seed`, abban az esetben, ha már van felhasználónk és törölni szeretnénk ezt a felhasználót vagy újra szeretnénk
seedelni az adatbázist úgy, hogy az összes felhasználót törölje, akkor `npm run seed --refresh` vagy `npm run seed:refresh`
-t kell használnunk. Ezt követően pedig tudjuk futtatni az API-t az `npm run start`-tal vagy az `npm run start:dev`-vel.

# Felhasználás

### Admin
Az admin seedelése után az adminból tudunk regisztrálni trainereket és clienteket.

`remove` button segítségével lehet törölni az adott felhasználót a `Registration` menü alatt.

`Remove client` segítségével pedig egy adott clientet tudunk törölni egy trainer alól.

`Add to trainer` gombbal pedig egy adott clientet tudunk adni hozzá egy trainerhez, ha hozzáadtuk a trainerhez
már többször nem lehet hozzáadni, de másik trainerhez igen.

### Trainer

###### Registration menüpont alatt
Miután az admin felvette a trainert, utána ő is tud clientet regisztrálni, ez esetben nincs `Add to trainer` gomb,
hanem automatikusan a trainerhez fog rendelődni az adott client.

A trainer nem tud véglegesen clientet törölni, mert lehet az adott client egy másik trainerhez tartozik, de a
`remove client` gombbal el tudja távolítani magától a clientet, ekkor a client nem fogja látni a trainer által megosztott
tartalmakat és a trainer sem látja a client dolgait.
###### Content menüpont alatt
A trainer itt tudja feltölteni azokat a képeket, videókat, amiket szeretne megosztani a clientekkel, hogy az adott
gyakorlatokat szabályosabban tudják végezni. Itt fontos, hogy a neveket úgy adjuk meg, hogy azok értelmesek legyenek,
így a clientek könnyebben tudnak keresni közöttük, hogy éppen melyik gyakorlatot szeretnék megnézni.
A trainerek feltölthetnek pdf-eket is, amikbe mehetnek receptek akár, ezzel is segítve az éppen diétázó clienteket, hogy
könnyebben tudják tartani a kajájukat.

##### Adott client
A trainer be tud lépni a clientbe, a nevére kattintva látni fogja, hogy az adott client-nek milyen célja van, ha változik
a célja, akkor a trainer ezen tud változtatni. ezen felül be tudja állítani, hogy csoportos vagy egyéni edzésen vesz részt
az edzettje. Valamint látja, hogy mennyi napja van hátra még a clientnek, abban az esetben, ha közeleg a lejárat, akkor
pirossal fogja jelezni (5 napnál kevesebb időnél). A lejárati időt meg tudja hosszabbítani, ahol négy féle csomag közül
választhat majd a client, ezek a: 30,90,180,360 napos csomagok.
Valamint be tudja állítani, hogy mennyi makrot vigyen be a client és ezeknek mennyi az összes kalória értéke.

Ezeket a funkciókat követően látja, hogy egy adott clientnek hogyan változik a súlya, amit a client fel tud venni bármikor,
ennek elemzésére egy grafikon lesz a trainer segítségére.

Túl nagy adathasználat esetén szükség lehet egy `remove all content`-re, de egyesével is tudja törölni a trainer a meglévő
vagy túl öreg fájlokat a clienttől.

### Client

##### Home menüpont alatt

A client az `add current weight`-tel tudja felvenni a jelenlegi súlyát, amit ad adott időben mért, ezek a grafikonon
neki is kirajzolódnak. Valamint az adott étkezésénél fel tudja venni, hogy mennyi makrot fogyaszott, ezeknek a bevitele
után automatikusan hozzáadódik az előzőhöz és levonódik a trainer által megadott mennyiségekből. Abban az esetben, ha
valamelyik szám 0 alá csökkenne, akkor jelzi a clientnek pirossal, hogy túlment valamelyik értéken. Ez esetben kalória
alapján tudja tovább számolni a maradék ételét, ha a kalóriát lépte túl, akkor ajánlott az adott napon nem enni, de ezt
a trainer fogja elmondani neki személyesen.
Abban az esetben, ha elrontaná valamelyik értéket, akkor a `reset intakes` gombbal vissza tudja állítani 0-ra az összes bevitt értéket.
Ha az `Expiration day` alatti szám 0-ra vagy az alá csökken, akkor a clientnek nem jelenik meg semmi.

##### Content menüpont alatt

A client itt találja a trainere által feltöltött dokumentumokat,képeket,videókat.

##### Asked videos menüpont alatt

A client itt tudja feltölteni a trainer által kért tartalmakat.

