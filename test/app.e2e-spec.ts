import { Test } from '@nestjs/testing';
import { HttpServer, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/modules/app/app.module';
import { Model } from 'mongoose';
import { UserService } from '../src/modules/user/domain/services/user.service';
import {
  adminStub,
  trainerStub,
} from '../src/modules/user/test/stubs/user.stub';
import { getModelToken } from '@nestjs/mongoose';
import { IBase } from '../src/modules/user/domain/models/interfaces/trainer.interface';
import { BaseUser } from '../src/modules/user/domain/models/base-user';
import { RoleEnum } from '../src/modules/user/domain/models/interfaces/role.enum';

describe('Application', () => {
  let httpServer: HttpServer;
  let app: INestApplication;
  let adminAccessToken: string;
  let user: IBase;
  let trainer: IBase;

  let userModel: Model<IBase>;

  let userService: UserService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();

    userModel = moduleRef.get<Model<IBase>>(getModelToken(BaseUser.name));

    userService = moduleRef.get<UserService>(UserService);

    httpServer = app.getHttpServer();
    await userModel.deleteMany({});
    user = await userService.createUser(adminStub());
    trainer = await userService.createUser(trainerStub());
    const userResponse = await request(httpServer).post('/auth/login').send({
      username: user.username,
      password: adminStub().password,
    });
    adminAccessToken = userResponse.body.token;
  });

  beforeEach(async () => {
    // await userModel.deleteMany({});
  });

  afterAll(async () => {
    await app.close();
  });

  describe('user', () => {
    it('should login an admin', async () => {
      const response = await request(httpServer).post('/auth/login').send({
        username: user.username,
        password: adminStub().password,
      });
      expect(response.status).toBe(201);
      expect(response.body.token).toBeDefined();
    });

    it('should login the trainer', async () => {
      const response = await request(httpServer).post('/auth/login').send({
        username: trainer.username,
        password: adminStub().password,
      });
      expect(response.status).toBe(201);
      expect(response.body.token).toBeDefined();
    });

    it('should find all user', async () => {
      const response = await request(httpServer)
        .get('/users')
        .set('Authorization', `Bearer ${adminAccessToken}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveLength(2);
    });

    it('should create a trainer', async () => {
      const response = await request(httpServer)
        .post('/users/trainers')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({
          username: 'trainer',
          password: '1234',
          config: {
            active: true,
            clients: [],
          },
          role: RoleEnum.TRAINER,
        });
      expect(response.status).toBe(201);
    });

    it('should return an unauthorized', async () => {
      const response = await request(httpServer).post('/users/trainers');
      expect(response.status).toBe(401);
    });

    it('should return a duplication error, trainer already exist', async () => {
      const response = await request(httpServer)
        .post('/users/trainers')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({
          username: 'trainer',
          password: '1234',
          config: {
            active: true,
            clients: [],
          },
          role: RoleEnum.TRAINER,
        });
      expect(response.status).toBe(409);
    });

    it('should update a trainer s password', async () => {
      const response = await request(httpServer)
        .patch(`/users/update/users/trainers/${trainer._id}/set/password`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send({
          password: '12345',
        });
      expect(response.status).toBe(200);
      expect(response.body.modifiedCount).toBe(1);
    });

    it('should delete a trainer', async () => {
      const response = await request(httpServer)
        .del(`/users/trainers/${trainer._id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`);
      expect(response.status).toBe(200);
      expect(response.body.deletedCount).toBe(1);
    });
  });
});
