import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import { Logger, LogLevel } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const openApiRoot = 'api-docs';
const mainLogger = new Logger('Main', { timestamp: true });

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    logger: ['log'] as LogLevel[],
  });

  const port = parseInt(process.env.PORT);
  const openApiConfig = new DocumentBuilder()
    .setTitle('Web Coaching')
    .setDescription('')
    .build();
  SwaggerModule.setup(
    openApiRoot,
    app,
    SwaggerModule.createDocument(app, openApiConfig),
  );

  await app.listen(port, () => {
    mainLogger.log(`Application is listening on port ${port}`);
  });
}

bootstrap().then(() =>
  mainLogger.log(
    `Bootstrap end, see documentation in /${openApiRoot} endpoint`,
  ),
);
