import { Seeder } from 'nestjs-seeder';
import { Model } from 'mongoose';
import { IBase } from '../../modules/user/domain/models/interfaces/trainer.interface';
import { InjectModel } from '@nestjs/mongoose';
import { RoleEnum } from '../../modules/user/domain/models/interfaces/role.enum';
import { BaseUser } from '../../modules/user/domain/models/base-user';
import { BcryptService } from '../../modules/app/services/bcrypt.service';

export class AdminSeeder implements Seeder {
  constructor(
    @InjectModel(BaseUser.name)
    private readonly adminModel: Model<IBase>,
    private readonly bcryptService: BcryptService,
  ) {}

  async drop(): Promise<any> {
    await this.adminModel.deleteMany().exec();
  }

  async seed(): Promise<any> {
    await this.adminModel.create({
      username: 'admin1',
      password: this.bcryptService.hashPassword('1234'),
      config: {
        lastLoggedIn: new Date(),
      },
      role: RoleEnum.ADMIN,
    });
  }
}
