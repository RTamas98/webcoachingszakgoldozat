import { seeder } from 'nestjs-seeder';
import { MongooseModule } from '@nestjs/mongoose';
import { AdminSeeder } from './admin.seeder';
import {
  BaseUser,
  userSchema,
} from '../../modules/user/domain/models/base-user';
import { BcryptService } from '../../modules/app/services/bcrypt.service';

seeder({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/web-coaching'),
    MongooseModule.forFeature([{ name: BaseUser.name, schema: userSchema }]),
  ],
  providers: [BcryptService],
}).run([AdminSeeder]);
