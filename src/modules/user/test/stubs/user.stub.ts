import { IBase } from '../../domain/models/interfaces/trainer.interface';
import { RoleEnum } from '../../domain/models/interfaces/role.enum';

export const adminStub = (): IBase => {
  return {
    password: '1234',
    role: RoleEnum.ADMIN,
    config: {
      lastLoggedIn: new Date(),
    },
    username: 'admin',
  };
};

export const trainerStub = (): IBase => {
  return {
    username: 'traineeeer',
    role: RoleEnum.TRAINER,
    config: {
      clients: [],
      active: true,
    },
    password: '1234',
  };
};
