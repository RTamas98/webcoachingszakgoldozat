import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { clientSchema } from './domain/models/clientSchema';
import { UserService } from './domain/services/user.service';
import { UserController } from './infrastructure/controllers/user.controller';
import { BaseUser, userSchema } from './domain/models/base-user';
import { RoleEnum } from './domain/models/interfaces/role.enum';
import { trainerSchema } from './domain/models/trainer.schema';
import { adminSchema } from './domain/models/admin.schema';
import { BcryptService } from '../app/services/bcrypt.service';
import { ErrorHandlerService } from '../app/helpers/error-handler.service';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constants/constants';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BaseUser.name,
        schema: userSchema,
        discriminators: [
          { name: RoleEnum.ADMIN, schema: adminSchema },
          { name: RoleEnum.TRAINER, schema: trainerSchema },
          { name: RoleEnum.CLIENT, schema: clientSchema },
        ],
      },
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),
  ],
  providers: [UserService, BcryptService, ErrorHandlerService],
  controllers: [UserController],
  exports: [UserService, MongooseModule],
})
export class UserModule {}
