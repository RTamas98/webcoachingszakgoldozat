import { Injectable } from '@nestjs/common';
import { Command, Option, Positional } from 'nestjs-command';
import { UserService } from '../../domain/services/user.service';

@Injectable()
export class UserCommand {
  constructor(private readonly userService: UserService) {}
  // changes password from command
  @Command({
    command: 'user:changePassword <username>',
    describe: 'change password for user',
  })
  async changePassword(
    @Positional({
      name: 'username',
      describe: 'the username',
      type: 'string',
    })
    login: string,
    @Option({
      name: 'password',
      describe: 'user password',
      type: 'string',
      alias: 'pw',
      required: true,
    })
    password: string,
  ) {
    await this.userService.changePassword(login, password);
  }
}
