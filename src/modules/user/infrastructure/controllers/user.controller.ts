import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserService } from '../../domain/services/user.service';
import { INutrients } from '../../domain/models/interfaces/nutrients.interface';
import { IBase } from '../../domain/models/interfaces/trainer.interface';
import { InjectModel } from '@nestjs/mongoose';
import { BaseUser, BaseUserDocument } from '../../domain/models/base-user';
import { Model } from 'mongoose';
import { RolesGuard } from '../../../auth/infrasctructure/guards/roles.guard';
import { RoleEnum } from '../../domain/models/interfaces/role.enum';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { GoalEnum } from '../../domain/models/interfaces/goal.enum';
import { JwtService } from '@nestjs/jwt';
import { ITrainingPlan } from '../../domain/models/interfaces/training-plan.interface';
import { Public } from '../../../auth/decorators/public.decorator';

@ApiTags('Users')
@UseGuards(RolesGuard)
@Controller('users')
export class UserController {
  constructor(
    @InjectModel(BaseUser.name)
    private readonly userModel: Model<BaseUserDocument>,
    private readonly userService: UserService,
    private jwtService: JwtService,
  ) {}

  @Roles(RoleEnum.CLIENT)
  @Patch('/clients/:username/weight')
  updateWeight(
    @Param('username') username: string,
    @Body() body: any,
  ): Promise<IBase> {
    return this.userService.updateWeight(username, body.weight);
  }

  @Roles(RoleEnum.CLIENT)
  @Patch('clients/:username/set/intakeOfFoods')
  updateIntake(
    @Param('username') username: string,
    @Body() body: INutrients,
  ): Promise<IBase> {
    return this.userService.updateIntakeOfNutrients(username, {
      fatInGram: body.fatInGram,
      carbInGram: body.carbInGram,
      proteinInGram: body.proteinInGram,
      foodInKcal: body.foodInKcal,
    });
  }

  @Roles(RoleEnum.TRAINER, RoleEnum.ADMIN)
  @Get()
  findAll(): Promise<IBase[]> {
    return this.userModel.find().exec();
  }

  //TODO transformationpipe
  @Roles(RoleEnum.ADMIN)
  @Post('/trainers')
  createTrainer(@Body() user: IBase): Promise<IBase> {
    return this.userService.createUser(user);
  }

  @Roles(RoleEnum.TRAINER, RoleEnum.ADMIN)
  @Post('/clients')
  async createClient(@Headers() headers, @Body() user: IBase): Promise<IBase> {
    const decodedToken: any = await this.jwtService.decode(
      headers.authorization.split(' ')[1],
    );
    return this.userService.createUser(
      user,
      decodedToken.id,
      decodedToken.role,
    );
  }

  @Roles(RoleEnum.TRAINER, RoleEnum.ADMIN)
  @Delete('/clients/:id')
  removeClient(@Param('id') id: string): Promise<IBase> {
    return this.userService.removeClient(id);
  }

  @Roles(RoleEnum.ADMIN)
  @Delete('/trainers/:id')
  removeTrainer(@Param('id') id: string): Promise<IBase> {
    return this.userService.removeTrainer(id);
  }

  @Roles(RoleEnum.TRAINER)
  @Patch('/trainers/:id/nutrients')
  updateNutrients(
    @Param('id') id: string,
    @Body() body: INutrients,
  ): Promise<IBase> {
    return this.userService.updateNutrientsOfClient(id, {
      fatInGram: body.fatInGram,
      carbInGram: body.carbInGram,
      proteinInGram: body.proteinInGram,
      foodInKcal: body.foodInKcal,
    });
  }

  @Roles(RoleEnum.TRAINER)
  @Patch('/clients/:id/expiration')
  updateExpirationDay(
    @Param('id') id: string,
    @Body() body: any,
  ): Promise<IBase> {
    return this.userService.updateExpirationDay(
      id,
      new Date(body.expirationDay),
    );
  }

  @Get('/trainers/find')
  findTrainers(): Promise<any> {
    return this.userModel.find({ role: 'trainer' }).exec();
  }

  @Get('/trainers/:username')
  findTrainer(@Param('username') username: string): Promise<any> {
    return this.userModel
      .findOne({ username: username })
      .populate('BaseUser')
      .populate('config')
      .exec();
  }

  @Get('/clients/:id')
  findClient(@Param('id') id: string): Promise<any> {
    return this.userModel.findOne({ _id: id }).exec();
  }

  @Get('/client/username/:username')
  findClientByUsername(@Param('username') username: string): Promise<any> {
    return this.userModel.findOne({ username: username }).exec();
  }

  @Get('/clients')
  findClients(): Promise<any> {
    return this.userModel.find({ role: 'client' }).exec();
  }

  @Get('/trainers/:username/clients')
  async findClientsOfATrainer(
    @Param('username') username: string,
  ): Promise<any> {
    const trainer = await this.userModel
      .findOne({ username: username })
      .lean()
      .exec();

    for (let i = 0; i < trainer.config.clients.length; i++) {
      trainer.config.clients[i] = await this.userModel
        .findOne({ _id: trainer.config.clients[i].toString() })
        .exec();
    }

    console.log(trainer);
    return trainer;
  }

  @Roles(RoleEnum.ADMIN, RoleEnum.TRAINER)
  @Patch(':trainerId/:clientId')
  async addToTrainer(
    @Param('trainerId') trainerId: string,
    @Param('clientId') clientId: string,
  ): Promise<any> {
    const trainer = await this.userModel.findOne({ _id: trainerId });
    const client = await this.userModel.findOne({ _id: clientId });
    return this.userService.addClientToTrainer(trainer, client);
  }

  @Roles(RoleEnum.ADMIN, RoleEnum.TRAINER)
  @Delete(':trainerName/:clientId')
  async removeClientFromTrainer(
    @Param('trainerName') trainerName: string,
    @Param('clientId') clientId: string,
  ): Promise<any> {
    const trainer = await this.userModel.findOne({ username: trainerName });
    const client = await this.userModel.findOne({ _id: clientId });
    return this.userService.removeClientFromTrainer(trainer, client);
  }

  @Roles(RoleEnum.TRAINER)
  @Patch('/clients/:id/:goal')
  updateGoal(@Param('id') id: string, @Param('goal') goal: GoalEnum) {
    return this.userService.updateGoal(id, goal);
  }

  @Roles(RoleEnum.TRAINER)
  @Patch('/clients/:id/set/:group')
  updateGroup(@Param('id') id: string, @Param('group') group: boolean) {
    return this.userService.updateGroup(id, group);
  }

  @Post('clients/trainingPlan/:id')
  createTrainingPlan(@Param('id') id: string, @Body() body: ITrainingPlan) {
    return this.userService.createTrainingPlan(id, body);
  }

  @Patch('patch/clients/trainingPlan/:id')
  updateTrainingPlan(@Param('id') id: string, @Body() body: any) {
    return this.userService.updateTrainingPlan(id, body);
  }

  @Delete('clients/trainingPlan/:id')
  removeTrainingPlanLine(@Param('id') id: string) {
    return this.userService.deleteTrainingPlan(id);
  }

  @Roles(RoleEnum.TRAINER, RoleEnum.ADMIN)
  @Patch('update/users/trainers/:id/set/password')
  updatePassword(@Param('id') id: string, @Body() body: any) {
    return this.userService.updatePassword(id, body);
  }
}
