import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IBase } from './interfaces/trainer.interface';
import { RoleEnum } from './interfaces/role.enum';

export type BaseUserDocument = BaseUser & Document;

@Schema({ discriminatorKey: 'role' })
export class BaseUser implements IBase {
  _id?: string;

  @Prop({ type: String, required: true, unique: true })
  username: string;

  @Prop({ type: String, required: true })
  password: string;

  @Prop({ type: Object, required: true })
  config: any;

  @Prop({ type: String, enum: RoleEnum, required: true })
  role: RoleEnum;

  @Prop({ type: [String], required: false })
  files: string[];
}

export const userSchema = SchemaFactory.createForClass(BaseUser);
