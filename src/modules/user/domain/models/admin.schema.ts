import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IAdmin } from './interfaces/admin.interface';
import { ApiProperty } from '@nestjs/swagger';
import { UserConfigModel } from './user-config-model';

@Schema()
class AdminConfig implements IAdmin {
  @ApiProperty()
  @Prop({ type: Date, required: true })
  lastLoggedIn: Date;
}

const adminConfigSchema = SchemaFactory.createForClass(AdminConfig);

export class Admin extends UserConfigModel {
  @Prop({ type: adminConfigSchema, required: true })
  config: AdminConfig;
}

export const adminSchema = SchemaFactory.createForClass(Admin);
