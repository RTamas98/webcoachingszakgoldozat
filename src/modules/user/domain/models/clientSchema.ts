import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { DayEnum } from './interfaces/day.enum';
import { GoalEnum } from './interfaces/goal.enum';
import { INutrients } from './interfaces/nutrients.interface';
import { IClient, IWeight } from './interfaces/user.interface';
import { RoleEnum } from './interfaces/role.enum';
import { UserConfigModel } from './user-config-model';
import { ITrainingPlan } from './interfaces/training-plan.interface';

@Schema({ _id: false })
class Nutrients implements INutrients {
  @Prop({ type: Number, required: true })
  carbInGram: number;

  @Prop({ type: Number, required: true })
  fatInGram: number;

  @Prop({ type: Number, required: true })
  foodInKcal: number;

  @Prop({ type: Number, required: true })
  proteinInGram: number;
}

const nutrientsSchema = SchemaFactory.createForClass(Nutrients);

@Schema({ _id: false })
class Weight implements IWeight {
  @ApiProperty()
  @Prop({ type: Date, required: true })
  date: Date;

  @ApiProperty()
  @Prop({ type: Number, required: true })
  weight: number;
}

const weightSchema = SchemaFactory.createForClass(Weight);

@Schema()
class TrainingPlan implements ITrainingPlan {
  @ApiProperty()
  @Prop({ type: String })
  id: string;

  @ApiProperty()
  @Prop({ type: String })
  name: string;

  @ApiProperty()
  @Prop({ type: String })
  range: string;

  @ApiProperty()
  @Prop({ type: String })
  dayIsIt: string;

  @ApiProperty()
  @Prop({ type: String })
  seriesNumber: string;

  @ApiProperty()
  @Prop({ type: String })
  C1C: string;

  @ApiProperty()
  @Prop({ type: String })
  C1R1: string;

  @ApiProperty()
  @Prop({ type: String })
  C1R2: string;

  @ApiProperty()
  @Prop({ type: String })
  C1R3: string;

  @ApiProperty()
  @Prop({ type: String })
  C1W1: string;

  @ApiProperty()
  @Prop({ type: String })
  C1W2: string;

  @ApiProperty()
  @Prop({ type: String })
  C1W3: string;

  @ApiProperty()
  @Prop({ type: String })
  C2C: string;

  @ApiProperty()
  @Prop({ type: String })
  C2R1: string;

  @ApiProperty()
  @Prop({ type: String })
  C2R2: string;

  @ApiProperty()
  @Prop({ type: String })
  C2R3: string;

  @ApiProperty()
  @Prop({ type: String })
  C2W1: string;

  @ApiProperty()
  @Prop({ type: String })
  C2W2: string;

  @ApiProperty()
  @Prop({ type: String })
  C2W3: string;

  @ApiProperty()
  @Prop({ type: String })
  C3C: string;

  @ApiProperty()
  @Prop({ type: String })
  C3R1: string;

  @ApiProperty()
  @Prop({ type: String })
  C3R2: string;

  @ApiProperty()
  @Prop({ type: String })
  C3R3: string;

  @ApiProperty()
  @Prop({ type: String })
  C3W1: string;

  @ApiProperty()
  @Prop({ type: String })
  C3W2: string;

  @ApiProperty()
  @Prop({ type: String })
  C3W3: string;

  @ApiProperty()
  @Prop({ type: String })
  C4C: string;

  @ApiProperty()
  @Prop({ type: String })
  C4R1: string;

  @ApiProperty()
  @Prop({ type: String })
  C4R2: string;

  @ApiProperty()
  @Prop({ type: String })
  C4R3: string;

  @ApiProperty()
  @Prop({ type: String })
  C4W1: string;

  @ApiProperty()
  @Prop({ type: String })
  C4W2: string;

  @ApiProperty()
  @Prop({ type: String })
  C4W3: string;

  @ApiProperty()
  @Prop({ type: String })
  set: string;
}

const trainingPlanSchema = SchemaFactory.createForClass(TrainingPlan);

@Schema({ _id: false })
class ClientConfig implements IClient {
  @ApiProperty()
  @Prop({ type: Date, required: true })
  expirationDay: Date;

  @ApiProperty()
  @Prop({ type: Boolean, required: true })
  group: boolean;

  @ApiProperty()
  @Prop({ type: [weightSchema], required: true })
  weight: IWeight[];

  @ApiProperty()
  @Prop({ enum: DayEnum, required: true })
  dayIsIt: DayEnum;

  @ApiProperty()
  @Prop({ enum: GoalEnum, required: true })
  goal: GoalEnum;

  @ApiProperty()
  @Prop({ type: nutrientsSchema, required: true })
  foodConsume: INutrients;

  @ApiProperty()
  @Prop({ type: nutrientsSchema, required: true })
  intakeOfNutrients: INutrients;

  @ApiProperty()
  @Prop({ type: String, enum: RoleEnum, required: true })
  role: RoleEnum.CLIENT;

  @ApiProperty()
  @Prop({ type: Number, required: true })
  age: number;

  @ApiProperty()
  @Prop({ type: [String], required: false })
  disease: string[];

  @ApiProperty()
  @Prop({ type: [trainingPlanSchema], required: false })
  trainingPlan: ITrainingPlan[];
}

const clientConfigSchema = SchemaFactory.createForClass(ClientConfig);

@Schema({ _id: false })
export class Client extends UserConfigModel {
  @Prop({ type: clientConfigSchema, required: true })
  config: ClientConfig;
}

export const clientSchema = SchemaFactory.createForClass(Client);
