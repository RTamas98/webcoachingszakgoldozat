import { ITrainer } from './interfaces/trainer.interface';
import { ApiProperty } from '@nestjs/swagger';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { UserConfigModel } from './user-config-model';
import * as mongoose from 'mongoose';
import { IClient } from './interfaces/user.interface';

@Schema({ _id: false })
class TrainerConfig implements ITrainer {
  @ApiProperty()
  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'BaseUser' }],
    required: true,
    default: [],
  })
  clients: IClient[];

  @ApiProperty()
  @Prop({ type: Boolean, required: true, default: true })
  active: boolean;
}

const trainerConfigSchema = SchemaFactory.createForClass(TrainerConfig);

@Schema({ _id: false })
export class Trainer extends UserConfigModel {
  @Prop({ type: trainerConfigSchema, required: true })
  config: TrainerConfig;
}

export const trainerSchema = SchemaFactory.createForClass(TrainerConfig);
