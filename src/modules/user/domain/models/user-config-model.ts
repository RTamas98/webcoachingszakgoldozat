import { RoleEnum } from './interfaces/role.enum';

export interface IUserConfig {
  _id: string;
  password: string;
  role: RoleEnum;
  username: string;
}

export class UserConfigModel implements IUserConfig {
  _id: string;
  password: string;
  role: RoleEnum;
  username: string;
}
