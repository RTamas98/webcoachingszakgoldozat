export enum GoalEnum {
  BULKING = 'bulking',
  STAGNATING = 'stagnating',
  DIET = 'diet',
}
