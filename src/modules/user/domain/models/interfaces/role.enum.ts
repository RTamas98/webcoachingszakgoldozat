export enum RoleEnum {
  'ADMIN' = 'admin',
  'CLIENT' = 'client',
  'TRAINER' = 'trainer',
}
