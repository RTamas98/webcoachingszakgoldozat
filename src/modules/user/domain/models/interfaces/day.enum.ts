export enum DayEnum {
  PUSH = 'push',
  PULL = 'pull',
  LEG = 'leg',
  REST = 'rest',
}
