import { RoleEnum } from './role.enum';
import { IClient } from './user.interface';

export interface IBase {
  _id?: string;
  username: string;
  password: string;
  config: any;
  role: RoleEnum;
  addToTrainer?: boolean;
}

export interface ITrainer {
  clients: IClient[];
  active: boolean;
}
