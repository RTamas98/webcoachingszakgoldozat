export interface INutrients {
  foodInKcal: number;
  carbInGram: number;
  proteinInGram: number;
  fatInGram: number;
}
