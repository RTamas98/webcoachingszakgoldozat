import { GoalEnum } from './goal.enum';
import { INutrients } from './nutrients.interface';
import { DayEnum } from './day.enum';
import { RoleEnum } from './role.enum';
import { ITrainingPlan } from './training-plan.interface';

export interface IClient {
  role: RoleEnum.CLIENT;
  dayIsIt: DayEnum;
  expirationDay: Date;
  group: boolean;
  foodConsume: INutrients;
  intakeOfNutrients: INutrients;
  goal: GoalEnum;
  weight: IWeight[];
  age: number;
  disease: string[];
  trainingPlan: ITrainingPlan[];
}

export interface IWeight {
  date: Date;
  weight: number;
}
