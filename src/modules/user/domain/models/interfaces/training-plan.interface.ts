export interface ITrainingPlan {
  id: string;
  dayIsIt: string;
  name: string;
  range: string;
  set: string;
  C1R1: string;
  C1W1: string;
  C1R2: string;
  C1W2: string;
  C1R3: string;
  C1W3: string;
  C1C: string;
  C2R1: string;
  C2W1: string;
  C2R2: string;
  C2W2: string;
  C2R3: string;
  C2W3: string;
  C2C: string;
  C3R1: string;
  C3W1: string;
  C3R2: string;
  C3W2: string;
  C3R3: string;
  C3W3: string;
  C3C: string;
  C4R1: string;
  C4W1: string;
  C4R2: string;
  C4W2: string;
  C4R3: string;
  C4W3: string;
  C4C: string;
}
