import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { INutrients } from '../models/interfaces/nutrients.interface';
import { BaseUser, BaseUserDocument } from '../models/base-user';
import { IBase } from '../models/interfaces/trainer.interface';
import { BcryptService } from '../../../app/services/bcrypt.service';
import { ErrorHandlerService } from '../../../app/helpers/error-handler.service';
import { GoalEnum } from '../models/interfaces/goal.enum';
import { ITrainingPlan } from '../models/interfaces/training-plan.interface';

@Injectable()
export class UserService {
  logger = new Logger(BaseUser.name);

  constructor(
    @InjectModel(BaseUser.name) private userModel: Model<BaseUserDocument>,
    private readonly bcryptService: BcryptService,
    private readonly errorHandler: ErrorHandlerService,
  ) {}

  // creates a user depends on the role
  async createUser(
    user: IBase,
    userId?: string,
    userRole?: string,
  ): Promise<IBase> {
    this.logger.log(user);
    const userCheck = await this.userModel.findOne({
      username: user.username,
    });
    if (userCheck) {
      this.errorHandler.conflict(user.username);
    }
    const password = this.bcryptService.hashPassword(user.password);
    const addUser = await this.userModel.create({ ...user, password });
    if (userRole === 'trainer') {
      const trainer = await this.userModel
        .findOne({ _id: userId })
        .lean()
        .exec();
      await this.addClientToTrainer(trainer, addUser);
    }
    return addUser;
  }

  // removes a client
  async removeClient(id: string): Promise<any> {
    await this.notFound(id);
    const allUser = await this.userModel.find({}).exec();
    const trainers = allUser.filter((user) => user.role === 'trainer');
    for (let i = 0; i < trainers.length; i++) {
      await this.removeClientFromTrainer(
        trainers[i]._id as any,
        new mongoose.Types.ObjectId(id) as any,
      );
    }
    return this.userModel.deleteOne({ _id: id }).exec();
  }

  // removes a trainer
  async removeTrainer(id: string): Promise<any> {
    await this.notFound(id);
    return this.userModel.deleteOne({ _id: id }).exec();
  }

  // add client to trainer
  addClientToTrainer(trainer: IBase, client: IBase): Promise<any> {
    return this.userModel
      .findOneAndUpdate(
        { _id: trainer._id },
        { $addToSet: { 'config.clients': client._id } },
      )
      .exec();
  }

  // removes a client from a trainer
  removeClientFromTrainer(trainer: IBase, client: IBase): Promise<any> {
    return this.userModel
      .findOneAndUpdate(
        { _id: trainer._id },
        { $pull: { 'config.clients': client._id } },
      )
      .exec();
  }

  // updates nutrients of a client
  async updateNutrientsOfClient(
    id: string,
    nutrients: INutrients,
  ): Promise<any> {
    await this.notFound(id);
    return this.userModel
      .findOneAndUpdate({ _id: id }, { 'config.foodConsume': nutrients })
      .exec();
  }

  // updates goal of a client
  async updateGoal(id: string, goal: GoalEnum): Promise<any> {
    await this.notFound(id);
    return this.userModel
      .findOneAndUpdate({ _id: id }, { 'config.goal': goal })
      .exec();
  }

  // updates expiration day of a client
  async updateExpirationDay(id: string, expirationDay: Date): Promise<any> {
    await this.notFound(id);
    return this.userModel
      .findOneAndUpdate({ _id: id }, { 'config.expirationDay': expirationDay })
      .exec();
  }

  // updates weight of a client
  updateWeight(username: string, weight: number): Promise<any> {
    return this.userModel
      .updateOne(
        { username: username },
        {
          $push: {
            'config.weight': { weight: weight, date: new Date() },
          },
        },
      )
      .exec();
  }

  // updates the client is a group member or not
  async updateGroup(id: string, group: boolean) {
    await this.notFound(id);
    return this.userModel.findOneAndUpdate(
      { _id: id },
      { 'config.group': group },
    );
  }

  // updates intake of nutrients of a client
  async updateIntakeOfNutrients(
    username: string,
    nutrients: INutrients,
  ): Promise<any> {
    await this.notFoundByUserName(username);
    return this.userModel
      .findOneAndUpdate(
        { username: username },
        { 'config.intakeOfNutrients': nutrients },
      )
      .exec();
  }

  // creates training plan
  async createTrainingPlan(id: string, plan: ITrainingPlan): Promise<any> {
    await this.notFound(id);
    return this.userModel.updateOne(
      { _id: id },
      {
        $push: {
          'config.trainingPlan': plan,
        },
      },
    );
  }

  // updates training plan
  async updateTrainingPlan(
    id: string,
    plan: Partial<ITrainingPlan>,
  ): Promise<any> {
    await this.notFound(id);
    const user = await this.userModel.findOne({ _id: id }).exec();
    for (let i = 0; i < user.config.trainingPlan.length; i++) {
      if (user.config.trainingPlan[i].id === plan.id) {
        user.config.trainingPlan[i] = plan;
      }
    }
    await user.save();
  }

  // deletes training plan
  async deleteTrainingPlan(id: string): Promise<any> {
    await this.notFound(id);
    const user = await this.userModel.findOne({ _id: id }).exec();
    user.config.trainingPlan = [];
    await user.save();
  }

  // not found error
  async notFound(id: string) {
    const user = await this.userModel.findOne({ _id: id });
    if (!user) {
      this.errorHandler.userNotFound(id);
    }
  }

  // not found error by username
  async notFoundByUserName(username: string) {
    const user = await this.userModel.findOne({ username });
    if (!user) {
      this.errorHandler.userNotFoundByUserName(username);
    }
  }

  // changes password (command)
  changePassword(username: string, password: string) {
    const hashedPassword = this.bcryptService.hashPassword(password);
    return this.userModel.findOneAndUpdate(
      { username: username },
      { password: hashedPassword },
    );
  }

  // changes password (used on ui)
  updatePassword(id: string, password: string) {
    console.log(Object.values(password)[0]);
    const hashedPassword = this.bcryptService.hashPassword(
      Object.values(password)[0],
    );
    return this.userModel.updateOne({ _id: id }, { password: hashedPassword });
  }
}
