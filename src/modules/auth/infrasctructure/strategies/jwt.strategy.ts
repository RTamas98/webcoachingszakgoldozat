import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstants } from '../../constants/constants';
import { IBase } from '../../../user/domain/models/interfaces/trainer.interface';
import { InjectModel } from '@nestjs/mongoose';
import { BaseUser } from '../../../user/domain/models/base-user';
import { Model } from 'mongoose';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectModel(BaseUser.name) private readonly userModel: Model<IBase>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  // jwt validation strategy
  validate(payload: { id: string }): Promise<IBase> {
    const { id } = payload;
    try {
      return this.userModel.findOne({ _id: id }).exec();
    } catch (err) {
      throw new UnauthorizedException('No token');
    }
  }
}
