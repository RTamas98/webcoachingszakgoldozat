import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { LocalAuthGuard } from '../guards/local-auth.guard';
import { Public } from '../../decorators/public.decorator';
import { InjectModel } from '@nestjs/mongoose';
import { BaseUser } from '../../../user/domain/models/base-user';
import { Model } from 'mongoose';
import { IBase } from '../../../user/domain/models/interfaces/trainer.interface';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    @InjectModel(BaseUser.name) private readonly userModel: Model<IBase>,
  ) {}

  // login
  @Public()
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Body() user: any): Promise<any> {
    const dbUser = await this.userModel
      .findOne({ username: user.username })
      .exec();
    console.log({
      username: dbUser.username,
      token: await this.authService.login(dbUser),
      role: dbUser.role,
    });
    return {
      username: dbUser.username,
      token: await this.authService.login(dbUser),
      role: dbUser.role,
    };
  }
}
