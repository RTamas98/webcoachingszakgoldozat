import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthGuardsNames } from '../../constants/constants';

@Injectable()
export class LocalAuthGuard extends AuthGuard(AuthGuardsNames.LOCAL) {}
