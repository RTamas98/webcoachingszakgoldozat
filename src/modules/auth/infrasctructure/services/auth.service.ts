import { Injectable, UnauthorizedException } from '@nestjs/common';
import { IBase } from '../../../user/domain/models/interfaces/trainer.interface';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import {
  BaseUser,
  BaseUserDocument,
} from '../../../user/domain/models/base-user';
import { Model } from 'mongoose';
import { BcryptService } from '../../../app/services/bcrypt.service';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(BaseUser.name)
    private readonly userModel: Model<BaseUserDocument>,
    private jwtService: JwtService,
    private readonly bcryptService: BcryptService,
  ) {}

  // this validates a user
  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userModel.findOne({ username }).exec();
    if (user) {
      const matched = this.bcryptService.comparePasswords(pass, user.password);
      if (matched) {
        return user;
      } else {
        throw new UnauthorizedException('Passwords do not match!');
      }
    }
  }

  // login
  async login(user: IBase): Promise<any> {
    const payload = {
      username: user.username,
      id: user._id,
      role: user.role,
    };
    return this.jwtService.sign(payload);
  }
}
