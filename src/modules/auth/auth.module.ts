import { Module } from '@nestjs/common';
import { AuthService } from './infrasctructure/services/auth.service';
import { UserService } from '../user/domain/services/user.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants/constants';
import { AuthController } from './infrasctructure/controllers/auth.controller';
import { LocalStrategy } from './infrasctructure/strategies/local.strategy';
import { MongooseModule } from '@nestjs/mongoose';
import { BaseUser, userSchema } from '../user/domain/models/base-user';
import { JwtStrategy } from './infrasctructure/strategies/jwt.strategy';
import { BcryptService } from '../app/services/bcrypt.service';
import { ErrorHandlerService } from '../app/helpers/error-handler.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BaseUser.name,
        schema: userSchema,
      },
    ]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),
  ],
  providers: [
    AuthService,
    UserService,
    LocalStrategy,
    JwtStrategy,
    BcryptService,
    ErrorHandlerService,
  ],
  controllers: [AuthController],
  exports: [],
})
export class AuthModule {}
