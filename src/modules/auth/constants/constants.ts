export const jwtConstants = {
  secret: 'secretKey',
};

export enum AuthGuardsNames {
  JWT = 'jwt',
  LOCAL = 'local',
}
