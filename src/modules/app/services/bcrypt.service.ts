import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BcryptService {
  // salt password
  hashPassword(password: string) {
    return bcrypt.hashSync(password, 12);
  }

  // compare the salted passwords
  comparePasswords(password: string, hash: string) {
    return bcrypt.compareSync(password, hash);
  }
}
