import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { SeederModule } from 'nestjs-seeder/dist/seeder/seeder.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '../auth/auth.module';
import { JwtAuthGuard } from '../auth/infrasctructure/guards/jwt-auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { ErrorHandlerService } from './helpers/error-handler.service';
import { FileModule } from './file/file.module';
import databaseConfig from './services/config/database.config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserCommand } from '../user/infrastructure/commands/user.command';
import { CommandModule } from 'nestjs-command';

@Module({
  imports: [
    // ConfigModule.forRoot({
    //   envFilePath: '.env',
    //   isGlobal: true,
    // }),
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
      load: [databaseConfig],
    }),
    MongooseModule.forRoot(process.env.MONGOURI + '/' + process.env.MONGODB),
    UserModule,
    AuthModule,
    SeederModule,
    FileModule,
    CommandModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    ErrorHandlerService,
    UserCommand,
  ],
})
export class AppModule {}
