import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Req,
  Res,
  StreamableFile,
  Headers,
} from '@nestjs/common';
import { FileService } from '../../domain/services/file.service';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import {
  BaseUser,
  BaseUserDocument,
} from '../../../../user/domain/models/base-user';
import { Model } from 'mongoose';
import { Public } from '../../../../auth/decorators/public.decorator';
import { Readable } from 'stream';
import { Roles } from '../../../../auth/decorators/roles.decorator';
import { RoleEnum } from '../../../../user/domain/models/interfaces/role.enum';

@Controller('files')
@ApiTags('files')
export class FileController {
  constructor(
    @InjectModel(BaseUser.name)
    private readonly userModel: Model<BaseUserDocument>,
    private readonly fileService: FileService,
    private jwtService: JwtService,
  ) {}

  // file uploader
  @Post()
  @ApiConsumes('multipart/form-data')
  async uploadFile(
    @Body() data: any,
    @Headers() headers,
    @Req() req,
  ): Promise<string | any> {
    const file = await this.fileService.writeFile(
      Readable.from(req.files.file.data),
      {
        filename: req.files.file.name,
        contentType: req.files.file.mimetype,
        metadata: {},
      },
    );
    const decodedToken: any = await this.jwtService.decode(
      headers.authorization.split(' ')[1],
    );
    await this.userModel.updateOne(
      { _id: decodedToken.id },
      { $push: { files: file._id } },
    );
    return file;
  }

  //file getter
  @Public()
  @Get(':id')
  async getFile(@Param('id') id: string, @Res() res: Response) {
    const info = await this.fileService.findById(id);
    res.header(
      'Content-Disposition',
      `attachment; filename="${info.filename}"`,
    );
    const readStream = await this.fileService.readStream(id);
    new StreamableFile(readStream).getStream().pipe(res);
  }

  // gets data from a file
  @Public()
  @Get(':id/info')
  async getInfo(@Param('id') id: string) {
    return this.fileService.findById(id);
  }

  // deletes all file
  @Roles(RoleEnum.TRAINER)
  @Delete(':id')
  async deleteFiles(@Param('id') id: string) {
    const user = await this.userModel.findOne({ _id: id }).exec();
    for (let i = 0; i < user.files.length; i++) {
      await this.fileService.delete(user.files[i]);
    }
    return this.userModel.findOneAndUpdate(
      { _id: id },
      { $pullAll: { files: user.files } },
    );
  }

  // removes a file by id
  @Roles(RoleEnum.TRAINER)
  @Delete('remove/:id')
  async deleteFile(@Param('id') id: string, @Headers() headers) {
    const decodedToken: any = await this.jwtService.decode(
      headers.authorization.split(' ')[1],
    );
    const user = await this.userModel.findOne({ _id: decodedToken.id }).exec();
    await this.fileService.delete(id);
    return this.userModel.findOneAndUpdate(
      { _id: user._id },
      { $pullAll: { files: [id] } },
    );
  }

  // removes all file from a client and the chunk of the file
  @Roles(RoleEnum.TRAINER)
  @Delete(':clientId/remove/clientsData/:id')
  async deleteFileFromClient(
    @Param('id') id: string,
    @Param('clientId') clientId: string,
  ) {
    const user = await this.userModel.findOne({ _id: clientId }).exec();
    await this.fileService.delete(id);
    return this.userModel.findOneAndUpdate(
      { _id: user._id },
      { $pullAll: { files: [id] } },
    );
  }
}
