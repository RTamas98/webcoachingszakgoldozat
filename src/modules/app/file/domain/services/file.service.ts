import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import {
  Filter,
  GridFSBucket,
  GridFSBucketReadStream,
  GridFSBucketWriteStreamOptions,
  GridFSFile,
} from 'mongodb';
import { Stream } from 'stream';
import { ObjectID } from 'bson';

export type WriteStreamOption = GridFSBucketWriteStreamOptions & {
  filename: string;
};

class AssetNotFoundError extends Error {
  constructor(filter: Filter<GridFSFile>) {
    super(`Asset not found with filter ${JSON.stringify(filter)}`);
  }
}

@Injectable()
export class FileService {
  private bucket: GridFSBucket;

  constructor(@InjectConnection() private readonly connection: Connection) {
    this.bucket = new GridFSBucket(connection.db, { bucketName: 'fs' });
  }

  // reads stream
  public async readStream(id: string): Promise<GridFSBucketReadStream> {
    const file = await this.findById(id);
    return this.bucket.openDownloadStream(file._id);
  }

  // writes file
  public writeFile(
    stream: Stream,
    options: WriteStreamOption,
  ): Promise<GridFSFile> {
    const { filename, ...streamOption } = options;
    return new Promise((resolve, reject) =>
      stream
        .pipe(this.bucket.openUploadStream(filename, streamOption))
        .on('error', async (err) => {
          reject(err);
        })
        .on('finish', async (file: GridFSFile) => {
          resolve(file);
        }),
    );
  }

  // finds a file by id
  public async findById(id: string): Promise<any> {
    return this.findOneOrFailed({ _id: new ObjectID(id) });
  }

  // catches error if the finding failed
  public async findOneOrFailed(
    filter: Filter<GridFSFile>,
  ): Promise<GridFSFile> {
    const file = await this.findOne(filter);
    if (file === null) {
      throw new AssetNotFoundError(filter);
    }
    return file;
  }

  // finds the filtered grid fs File
  public async findOne(filter: Filter<GridFSFile>): Promise<GridFSFile> {
    const results = await this.find(filter);
    if (results.length === 0) {
      return null;
    }
    return results.shift();
  }

  // find the grid fs files (array)
  public async find(filter: Filter<GridFSFile>): Promise<GridFSFile[]> {
    return await this.bucket.find(filter).toArray();
  }

  // removes one grid fs file and the chunks of the file
  public async delete(id: string): Promise<boolean> {
    const file = await this.findById(id);
    return new Promise<boolean>((resolve, reject) => {
      this.bucket.delete(file._id, async (err) => {
        if (err) {
          reject(err);
        }
        resolve(true);
      });
    });
  }

  // removes all grid fs files and chunks
  public async deleteMany(ids: string[]): Promise<boolean> {
    return new Promise<boolean>(async (resolve, reject) => {
      for (let i = 0; i < ids.length; i++) {
        const file = await this.findById(ids[i]);
        this.bucket.delete(file._id, async (err) => {
          if (err) {
            reject(err);
          }
          resolve(true);
        });
      }
    });
  }
}
