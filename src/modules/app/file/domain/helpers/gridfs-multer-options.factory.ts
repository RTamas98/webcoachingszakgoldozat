import {
  MulterModuleOptions,
  MulterOptionsFactory,
} from '@nestjs/platform-express';
import { ConfigService } from '@nestjs/config';
import { MongooseModuleOptions } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { GridFsStorage } from 'multer-gridfs-storage/lib/gridfs';
import { DATABASE } from '../../../services/config/constants';

@Injectable()
export class GridFsMulterOptionsFactory implements MulterOptionsFactory {
  private readonly gridFsStorage: GridFsStorage;

  constructor(private readonly configs: ConfigService) {
    const dbConfig: MongooseModuleOptions =
      configs.get<MongooseModuleOptions>(DATABASE);
    this.gridFsStorage = new GridFsStorage({
      url: process.env.MONGOURI,
      file: (req, file) => {
        return {
          ...file,
          filename: file.originalname?.trim(),
        };
      },
    });
  }

  createMulterOptions(): MulterModuleOptions {
    return {
      storage: this.gridFsStorage,
    };
  }
}
