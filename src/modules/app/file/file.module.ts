import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { FileController } from './infrastructure/controllers/file.controller';
import { FileService } from './domain/services/file.service';
import { MulterModule } from '@nestjs/platform-express';
import { GridFsMulterOptionsFactory } from './domain/helpers/gridfs-multer-options.factory';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from '../../user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../auth/constants/constants';
import * as expressFileUpload from 'express-fileupload';

@Module({
  imports: [
    MulterModule.registerAsync({
      useClass: GridFsMulterOptionsFactory,
    }),
    UserModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),

    MongooseModule.forRoot(process.env.DB_URI),
  ],
  controllers: [FileController],
  providers: [FileService, GridFsMulterOptionsFactory],
})
export class FileModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(expressFileUpload()).forRoutes({
      path: '/files',
      method: RequestMethod.POST,
    });
  }
}
