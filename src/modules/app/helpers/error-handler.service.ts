import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';

@Injectable()
export class ErrorHandlerService extends HttpException {
  logger = new Logger(ErrorHandlerService.name);

  // http status : 403
  conflict(username: string) {
    this.logger.error(`This username already exists ${username}`);
    throw new HttpException(
      {
        status: HttpStatus.CONFLICT,
        error: `This username already exists: ${username}`,
      },
      HttpStatus.CONFLICT,
    );
  }

  // http status bad request
  badRequest() {
    this.logger.error('Bad Request');
    throw new HttpException(
      {
        status: HttpStatus.BAD_REQUEST,
        error: 'Bad request',
      },
      HttpStatus.BAD_REQUEST,
    );
  }

  // http status 401
  unauthorized() {
    this.logger.error('Unauthorized');
    throw new HttpException(
      {
        status: HttpStatus.UNAUTHORIZED,
        error: 'Unauthorized',
      },
      HttpStatus.UNAUTHORIZED,
    );
  }

  // this user does not exist by id
  userNotFound(_id: string) {
    this.logger.error(`This user is not found with id: ${_id}`);
    throw new HttpException(
      {
        status: HttpStatus.BAD_REQUEST,
        error: `This user is not found with id: ${_id}`,
      },
      HttpStatus.BAD_REQUEST,
    );
  }

  // this user does not exist by username
  userNotFoundByUserName(username: string) {
    this.logger.error(`This user is not found with username: ${username}`);
    throw new HttpException(
      {
        status: HttpStatus.BAD_REQUEST,
        error: `This user is not found with username: ${username}`,
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}
